import json

def checkadmin(admin_id):
   with open('admins.json', 'r') as f: 
      admins = json.load(f)
      
   return admin_id in admins["admins"] 


def register_admin(admin_id):
   with open('admins.json', 'r') as f: 
      admins = json.load(f)
      
   admins["admins"].append(admin_id)
   
   with open(f'admins.json', 'w') as f:
      json.dump(admins, f, ensure_ascii=False, indent=4)
   
   return True

def get_all_admins():
   with open('admins.json', 'r') as f: 
      admins = json.load(f)

   return admins["admins"]

# print(checkadmin(339388424))
# print(register_admin(339388424))