import telebot
import time
import schedule
# from multiprocessing.context import Process
# import datetime
from threading import Thread
from check import check
import json
import admin

# from __future__ import print_function
# from telebot import types
# from datetime import datetime
# from desktopmagic.screengrab_win32 import (
# 	getDisplayRects, saveScreenToBmp, saveRectToBmp, getScreenAsImage,
# 	getRectAsImage, getDisplaysAsImages)
# from datetime import datetime


API_TOKEN = '1101323100:AAEQvWTMqhNmW0auFBotdKoSawG7goCO24Q' #test
# API_TOKEN = '5632277246:AAFBoAuGSNBmIaicWTTfnQMcOW4NqqeTo-Y' #main
Group = 339388424 #my id
PASSWORD = "ebis2001"
# Group = -792420435 #group id

bot = telebot.TeleBot(API_TOKEN)



def admin_registrate(message): 
   try: 
      if message.text == PASSWORD:
         admin.register_admin(message.chat.id)
         bot.send_message(message.chat.id, f'Вы зарегестрированы - ура!')
      else:
         bot.send_message(message.chat.id, f'Пароль неверный - чтобы попробовать снова введите /start')
   
   except: 
      print("Ошибка в start")

@bot.message_handler(commands=['start', 'register'])
def start_func(message):
   try:
      if(admin.checkadmin(message.chat.id)): 
         bot.send_message(message.chat.id, f'Рад приветствовать снова!')
      else: 
         send = bot.send_message(message.chat.id, 'Похоже, вы новенький, введите пароль: ')
         bot.register_next_step_handler(send, admin_registrate)
      
   except: 
      print("Ошибка в start")
   
   

@bot.message_handler(commands=['stop'])
def stop_bot(message):
   try:
      with open('config.json', 'r') as f: 
         conf = json.load(f)
         
      conf["work"] = False
         
      with open(f'config.json', 'w') as f:
         json.dump(conf, f, ensure_ascii=False, indent=4)
      
      bot.send_message(message.chat.id, f'Остановили проверки')
      send_all_info()
   except:
      bot.send_message(message.chat.id, f'Что-то пошло не так')
      print('error in stop_bot')
      
      

@bot.message_handler(commands=['run'])
def stop_bot(message):
   try:
      if not admin.checkadmin(message.chat.id): 
         return start_func(message)
      
      with open('config.json', 'r') as f: 
         conf = json.load(f)
         
      conf["work"] = True
         
      with open(f'config.json', 'w') as f:
         json.dump(conf, f, ensure_ascii=False, indent=4)
      
      bot.send_message(message.chat.id, f'Возобновили проверки')
      send_all_info()
   except:
      bot.send_message(message.chat.id, f'Что-то пошло не так')
      print('error in stop_bot')
      


def send_all_info():
   try:
      with open('config.json', 'r') as f: 
         conf = json.load(f)
         
      if conf["work"]:
         response = f"Бот работает.\nПроверки установлены на каждые: {conf['check_time']} секунд\nОт паузы осталось: {conf['pause']} секунд"
      else: 
         response = f"Бот не работает.\nПроверки установлены на каждые: {conf['check_time']} секунд\nОт паузы осталось: {conf['pause']} секунд"
      
      admins = admin.get_all_admins()
      
      for admin_1 in admins: 
         bot.send_message(admin_1, response)
      
   except: 
      print('error in send_all_info')


@bot.message_handler(commands=['info'])
def send_info(message):
   try: 
      if not admin.checkadmin(message.chat.id): 
         return start_func(message)
      
      with open('config.json', 'r') as f: 
         conf = json.load(f)
         
      if conf["work"]:
         response = f"Бот работает.\nПроверки установлены на каждые: {conf['check_time']} секунд\nОт паузы осталось: {conf['pause']} секунд"
      else: 
         response = f"Бот не работает.\nПроверки установлены на каждые: {conf['check_time']} секунд\nОт паузы осталось: {conf['pause']} секунд"
      
      bot.send_message(message.chat.id, response)
   except:
      bot.send_message(message.chat.id, 'Что-то пошло не так')
      print('error in send_info')


@bot.message_handler(commands=['pause'])
def pause(message):
   try:
      if not admin.checkadmin(message.chat.id): 
         return start_func(message)
      
      with open('config.json', 'r') as f: 
         conf = json.load(f)
      
      conf["pause"] = 3600
      
      with open(f'config.json', 'w') as f:
         json.dump(conf, f, ensure_ascii=False, indent=4)
      
      bot.send_message(message.chat.id, 'Хорошо, я отдохну часок')
      send_all_info()
   except:
      bot.send_message(message.chat.id, f'Что-то пошло не так')
      print('error in pause')


def set_check_time_final(message):
   try:
      if message.text == "Отмена":
         bot.send_message(message.chat.id, f'Хорошо, отменили')
         return False
      
      try:
         int(message.text)
      except: 
         bot.send_message(message.chat.id, f'Вы ввели не число')
         return False
      
      with open('config.json', 'r') as f: 
         conf = json.load(f)
         
      conf["check_time"] = int(message.text)
         
      with open(f'config.json', 'w') as f:
         json.dump(conf, f, ensure_ascii=False, indent=4)

      bot.send_message(message.chat.id, f'Теперь проверки проходят каждые {message.text} секунд')
      send_all_info()
   except:
      bot.send_message(message.chat.id, f'Что-то пошло не так')
      print('error in set_check_time_final')

@bot.message_handler(commands=['set_check'])
def set_check_time(message):
   try:
      if not admin.checkadmin(message.chat.id): 
         return start_func(message)
      
      send = bot.send_message(message.chat.id, 'Введите, как часто делать проверки (в секундах):')
      bot.register_next_step_handler(send, set_check_time_final)
   except:
      bot.send_message(message.chat.id, f'Что-то пошло не так')
      print('error in set_check_time')


def set_pause_time_final(message):
   try:
      if message.text == "Отмена":
         bot.send_message(message.chat.id, f'Хорошо, отменили')
         return False
      
      try:
         int(message.text)
      except: 
         bot.send_message(message.chat.id, f'Вы ввели не число')
         return False
      
      with open('config.json', 'r') as f: 
         conf = json.load(f)
         
      conf["pause"] = int(message.text)
         
      with open(f'config.json', 'w') as f:
         json.dump(conf, f, ensure_ascii=False, indent=4)

      bot.send_message(message.chat.id, f'Установлена пауза на {message.text} секунд')
      send_all_info()
   except:
      bot.send_message(message.chat.id, f'Что-то пошло не так')
      print('error in set_pause_time_final')


@bot.message_handler(commands=['set_pause'])
def set_pause_time(message):
   try:
      if not admin.checkadmin(message.chat.id): 
         return start_func(message)
      
      send = bot.send_message(message.chat.id, 'Введите, сколько будет длиться пауза:')
      bot.register_next_step_handler(send, set_pause_time_final)
   except:
      bot.send_message(message.chat.id, f'Что-то пошло не так')
      print('error in set_pause_time')
      
      
# @bot.message_handler(commands=['check_screens'])
# def check_screens(message):
#    for displayNumber, im in enumerate(getDisplaysAsImages(), 1):
#       now = datetime.now().strftime('%d.%m.%Y %H:%M:%S')

#       im.save(f'src_{displayNumber}.png', format='png')
#       with open(f'src_{displayNumber}.png', 'rb') as image:
#          bot.send_document(message.chat.id, image, caption=f"{now}")


@bot.message_handler(commands=['check_t'])
def check_t(message):
   try: 
      res = check(True)
      # print(res)
            
      bot.send_message(message.chat.id, '\n'.join(res))
            
   except:
      bot.send_message(message.chat.id, f'Что-то пошло не так')
      print('error in check_t')
      
      

@bot.message_handler(commands=['chat_id'])
def get_id_by_message(message):
   bot.send_message(message.chat.id, message.chat.id)


@bot.message_handler(commands=['comands'])
def send_comands(message):
   
   comands_text = """/start - начало работы 
/run - запустить проверки 
/stop - остановить проверки 
/info - информация о работе бота 
/pause - пауза на час
/set_pause - установить паузу
/set_time - установить частоту проверок
/check_t - проверить температуру
/check_screens - проверить экраны"""

   bot.send_message(message.chat.id, comands_text)


def temp_check(): 
   with open('config.json', 'r') as f: 
      conf = json.load(f)
      
   if not conf["work"]:
      return False
      
   if conf["pause"]:
      conf["pause"] -= 1
      
      with open(f'config.json', 'w') as f:
         json.dump(conf, f, ensure_ascii=False, indent=4)
         
      return False
   
   # print( int(time.time() % conf["check_time"]) )
   
   if int(time.time() % conf["check_time"]):
      return False
   
   res = check()
   # print(res)
   if res: 
      admins = admin.get_all_admins()
      
      for admin_1 in admins: 
         bot.send_message(admin_1, '\n'.join(res))
   

def sending():
   schedule.every(1).seconds.do(temp_check)
   # schedule.every(min).hours.do(check_prices)
   while True:
      schedule.run_pending()
      time.sleep(1)

if __name__ == '__main__':
   my_thread = Thread(target=sending)
   my_thread.start()
   bot.infinity_polling()