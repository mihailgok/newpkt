import xml.etree.ElementTree as ET
import requests
import shutil
# import telebot
# import time

# API_TOKEN = '1101323100:AAEQvWTMqhNmW0auFBotdKoSawG7goCO24Q' #test
# API_TOKEN = '5632277246:AAFBoAuGSNBmIaicWTTfnQMcOW4NqqeTo-Y' #main
# Group = 339388424
# Group = -792420435

# bot = telebot.TeleBot(API_TOKEN)

COEFF_NAMES = {
   "R1": "Стр.", 
   "R2": "Спаи.", 
   "R3": "Пер.", 
   "R4": "Сол.", 
}


def get_min_max():
   T_range_file = ET.parse('range.xml')
   T_range_raw = T_range_file.getroot()
   min = 0
   max = 0

   for child in T_range_raw:
      if child.tag == "MIN":
         min = int(child.text)
      elif child.tag == "MAX":
         max = int(child.text)
         
   return (min, max)

def get_coeff():
   # coeff_file = requests.get('https://10.10.10.61/measurments.xml', stream=True)
   # with open('coeff.xml', 'wb') as out_file:
   #    shutil.copyfileobj(coeff_file.raw, out_file)
   
   coeff_all = ET.parse('coeff.xml')
   coeff_root = coeff_all.getroot()

   all_k = {}
   for child in coeff_root:
      new_mas = []
      for k in child:
         new_mas.append(float(k.text.replace(",", ".")))
      
      all_k[child.tag] = new_mas
      
   return all_k
   

def check(just_check = False):
   all_k = get_coeff()
   minmax = get_min_max()
   
   measure = ET.parse('measurements.xml')
   measure_root = measure.getroot()

   measure_needed = ["R1", "R2", "R3", "R4"]

   measure_obj = {}

   for child in measure_root:
      if child.tag in measure_needed:
         measure_obj[child.tag] = float(child.text.replace(",", "."))

   R0 = 1000

   T_final = {}
   
   final_info = []

   for key in measure_obj:
      coeff = R0/(measure_obj[key])
      T = 0
      i = 0
      for k in all_k[key]:
         T = round(T + k * pow(coeff, i), 5)
         i = i + 1
      
      T_final[key] = T
      
      # print(T)
      
      if T < minmax[0] or T > minmax[1]:
         final_info.append(f"{ '' if just_check else 'Внимание!'}\n{COEFF_NAMES[key]} = {round(T, 2)} K\n")
         # bot.send_message(Group, f"Температура вне диапазона!\nЗначение на {key} - {T}", parse_mode="HTML")
         
   return final_info
   
# bot.send_message(Group, "", parse_mode="HTML")
# while True:
#    print(check())
#    time.sleep(10)
      
